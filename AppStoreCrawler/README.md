# AppSotreCrawler
## Overview ##
This project is to develop a framework for crawling app metadata like overview, ratings, reviews from mobile app stores. It currently supports iTunes and GooglePlay. 
It can be easily extended to support other mobile app stores like Windows App Store by writing new webpage parsers.
Besides, it supports dynamic IP address change by setting up proxy servers using Tor (Configuration to be shared).

## INSTALLATION ##
-------------
The installation includes three parts that are required of Large-scale-detector. 
### Platforms
   We recommend Linux/Unix. 

### Database
   We recommend MySQL 5.5 for the database of apps' information. 
   It is also allowed to use SQLite but it would get lower performance.

### JRE
   We recommend JRE 1.7 for running Large-scale-detector. 

### AppSotreCrawler
   It contains both the source codes and the installation including GooglePlay and AppleItunes. 

   For source code, you can directly use jetbrain to load the entire folder ./AppStoreCrawler. To generate jar file, please configure artifacts.
   
   In the directory of ./AppStoreCrawler/install, I have generate a jar file and you can directly run them by excuting ./AppStoreCrawler/install/{googlePlayScript,ituneScript}/*.sh after configuring the conf/database.properties and conf/log4j.properties, and ./AppStoreCrawler/install/{googlePlayScript,ituneScript}/init_database/createDatabase.sh. 

### Run the source code
   1. Download the entire directory
   2. Import into jetbrain as a project
   3. Add all the jar files in lib/ as the dependency
   4. Change files
   
        1) conf/database.properties: Change the sqlite file to your own path
        
            DS_db1_URL=jdbc:sqlite:{your own path/xxx.db}
            
        2) conf/log4j.properties
        
            log4j.appender.FILE.File={your own load file}
            
   5. Init database
   
        $ sh init_database/createDatabase.sh

### File Description

```
|-- AppStoreCrawler.iml //workspace file of Intellij
|-- README.md
|-- conf
|   |-- database.properties # The configuration of AppStoreCrawler. 
|   -- log4j.properties # The configuration of the logging subsystem. Please DO change the path to the log file.
|-- install # The installation package of GooglePlay and AppItunes
|   |-- googlePlayScript
|   |   |-- GooglePlayCrawler.jar # jar file of source code
|   |   |-- META-INF 
|   |   |   -- MANIFEST.MF # The configration of JAR generation.
|   |   |-- conf # The same as conf in source code
|   |   |   |-- database.properties 
|   |   |   -- log4j.properties
|   |   |-- init_database # Database initialization.
|   |   |   |-- createDatabase.sh 
|   |   |   |-- mysql.sql
|   |   |   -- sqlite.sql
|   |   |-- restart.sh #restart the crawler
|   |   |-- shutdownMonitor.sh # shutdown the crawler
|   |   |-- startMonitor.sh # start the crawler
|   |   -- traffic.sh # show the current traffic
|   -- ituneScript
|       |-- AppleItuneCrawler.jar
|       |-- META-INF
|       |   -- MANIFEST.MF
|       |-- conf
|       |   |-- database.properties
|       |   -- log4j.properties
|       |-- init_database
|       |   |-- createDatabase.sh # please run this script to initialize database for the crawler: creating tables, load categroies 
|       |   |-- itunes_categories.txt
|       |   |-- itunes_storefront.txt
|       |   |-- load_itunes.py
|       |   |-- mysql.sql
|       |   -- sqlite.sql
|       |-- restart.sh 
|       |-- shutdownMonitor.sh
|       |-- startMonitor.sh
|       -- traffic.sh
|-- lib # jar libs that needed to run the source code of crawler
|   |-- JavaTorController.jar
|   |-- commons-lang3-3.3.2.jar
|   |-- json_lib.jar
|   |-- rating_libV2.jar
|   -- sqlite-jdbc-3.15.1.jar
|-- logs # log file, please specify the path to the log in log4j.properties
|   -- log 
-- src # source code of crawler
    |-- DAO_DB # data object related java files
    |   |-- ConfigurationException.java
    |   |-- DBMonitor.java
    |   |-- DataControl.java
    |   |-- DataControlException.java
    |   |-- DataManager.java
    |   |-- DataManagerException.java
    |   |-- DbObject.java
    |   |-- FlushToDB.java # this file is to control the thread of database flushing
    |   |-- MessageManager.java
    |   |-- NoImplementationException.java
    |   -- SQLiteDialect.java
    |-- configuration # the configuration class
    |   |-- BaseConfig.java
    |   -- CrawlerConfig.java
    |-- crawlerAgent # the crawler agent which will generate http connect to the target websites. You must extend Crawler to accommendate new app stores.
    |   |-- AppleItuneCrawler.java
    |   |-- Crawler.java
    |   -- GooglePlayCrawler.java
    |-- crawlerEngine 
    |   |-- CommentCollector.java
    |   |-- CrawlerEngine.java
    |   |-- DeveloperProfileEngine.java
    |   |-- IdEngine.java
    |   |-- OverviewEngine.java
    |   |-- RankEngine.java
    |   -- UserProfileEngine.java
    |-- crawlerParser # the crawler parser of retrieve app metadata. You must extend Crawler to accommendate new app stores.
    |   |-- AppleItuneParser.java
    |   |-- ContentParser.java
    |   -- GooglePlayParser.java
    |-- data # the data objects
    |   |-- AppData.java
    |   |-- AppId.java
    |   |-- AppRatingInfo.java
    |   |-- Comment.java
    |   |-- Developer.java
    |   |-- EngineStatus.java
    |   |-- NodeStatus.java
    |   -- Reviewer.java
    -- utility
        |-- DataPersistence.java
        |-- MultiDateFormat.java
        |-- MyProxySelector.java
        |-- SystemLogger.java
        -- SystemLoggerFactory.java
```