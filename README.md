# Fake Review Detective
## Overview ##
This repository is to share the tools of downloading app reviews from popular app stores (e.g., iTunes, GooglePlay), and source code/data of my research papers listed below. I will maintain all the code and please follow this repository for updates.

## AppStoreCrawler ##
This is a framework for downloading mobile app store views from iTunes and GooglePlay. It can be easily extended to support other mobile app stores by writing specific parsers for the new websites. All our papers relied on this crawler to download app metadata.

Current version supports downloading app metadata including overview, review, reviewer profile etc. 

The framework will download and parse the specified information and store them into databases. The databases that this framework can support includes Sqlite3, MySQL 5.0+. 

## AppWatcher ##
AppWatcher[2] is our research about the underground market of trading mobile app reviews. I have uploaded the data (i.e., *.xls) we have collected by monitoring websites like www.bestreviewapp.com. The folder of AppWatcher/appTracer contains the source code of the app tracer we have proposed in the paper.

## AbusedAppDetector ##
The AbusedAppDetector is an implementation of our paper[1], which relies on AppStoreCrawler to download app review and reviewer profile on the fly. 


## Research Papers Related to Fake Review Detection ##
Please cite our papers if you use the source code or the data.

[1] Z. Xie, S. Zhu, Q.Li and W. Wang. You Can Promote, But You Can’t Hide: Large-Scale Abused App Detection in Mobile App Stores. Proceedings of Annual Computer Security Applications Conference (ACSAC), 2016.

[2] Z. Xie and S. Zhu. AppWatcher: Unveiling the Underground Market of Trading Mobile App Reviews. Proceedings of the 8th ACM Conference on Security and Privacy in Wireless and Mobile Networks (WiSec), 2015.

[3] Z. Xie and S. Zhu. GroupTie: Toward Hidden Collusion Group Discovery in App Stores. Proceedings of the 7th ACM Conference on Security and Privacy in Wireless and Mobile Networks (WiSec), 2014.