# AppSotreCrawler
## Overview ##
This project is to develop a framework for crawling app metadata like overview, ratings, reviews from mobile app stores. It currently supports iTunes and GooglePlay. 
It can be easily extended to support other mobile app stores like Windows App Store by writing new webpage parsers.
Besides, it supports dynamic IP address change by setting up proxy servers using Tor (Configuration to be shared).

## File description ##
app_data.xls : app meta data crawled from target websits.

app_reviews.xls : all the reviews of the apps in app_data.xls.

appTracer: the source code of app tracer.


