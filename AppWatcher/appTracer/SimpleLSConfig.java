package simpleLS;

import configuration.BaseConfig;

import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class SimpleLSConfig extends BaseConfig {

	private volatile int threadCount=20;
	/*Model parameters See large-scale paper Table I*/

	private float thresholdReviewSize=100;//the minimum reviews of one customer
	private float thresholdAppSize=2;//
	private float largestReviewers=3000;
	private float poplarAppReviewerSize=15000;
	private String seedStoreLocation;
	private int dataSource=1;
    private String presetSeedFile =null;
	private static SimpleLSConfig config;
	public static SimpleLSConfig getInst() {
		if (config == null) {
			config = new SimpleLSConfig();
			config.init();
		}
		return config;
	}

	@Override
	protected void dynamicLoading() {
	}
	
	protected void init(){
		try{
			this.threadCount=Integer.parseInt(super.properties.getProperty("LS_R_th_count","20").trim());  
            this.presetSeedFile =super.properties.getProperty("LS_seed_file","").trim();
            this.seedStoreLocation=super.properties.getProperty("seedStoreLocation","143441");
            this.dataSource=Integer.parseInt(super.properties.getProperty("data_source","1").trim());
			this.thresholdReviewSize=Integer.parseInt(super.properties.getProperty("LS_N_b","100").trim());
			this.thresholdAppSize=Integer.parseInt(super.properties.getProperty("LS_N_a","2").trim());  
			this.largestReviewers=Integer.parseInt(super.properties.getProperty("LS_N_r","3000").trim());
			this.poplarAppReviewerSize=Integer.parseInt(super.properties.getProperty("LS_N_p","15000").trim());  

		}catch(Exception ex){
			log.error(ex.getMessage());
			System.exit(0);
		}
	}
	//getters
	public int getThreadCount() {
		return threadCount;
	}


	public String getSeedStoreLocation() {
		return seedStoreLocation;
	}


	public float getThresholdReviewSize() {
		return thresholdReviewSize;
	}

	public float getThresholdAppSize() {
		return thresholdAppSize;
	}

	public float getLargestReviewers() {
		return largestReviewers;
	}

	public float getPoplarAppReviewerSize() {
		return poplarAppReviewerSize;
	}


	public boolean isAllowWebpage(){
		return this.dataSource==2;
	}

    public String getPresetSeedFile(){
        return this.presetSeedFile;
    }
}
