package simpleLS;

import crawlerAgent.AppleItuneCrawler;
import crawlerAgent.Crawler;
import crawlerEngine.CommentCollector;
import crawlerEngine.OverviewEngine;
import crawlerEngine.UserProfileEngine;
import data.AppData;
import data.Comment;
import data.Reviewer;
import largeScaleDetector.FlushToDBLS;
import utility.DataPersistence;
import utility.SystemLogger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by xiezhen on 8/20/2014.
 */
public class SimpleLS {
    private static SystemLogger log = SystemLogger.getLogger(SimpleLS.class);

    private Crawler crawler=null;
    //store confirmed abused apps and attackers
    private HashSet<String> confirmedAppSet = new HashSet<String>();
    private HashSet<String> confirmedAttackerSet=new HashSet<String>();
    //store unconfirmed apps and attackers
    private HashMap<String,HashSet<String>> unconfirmedAppSet = new HashMap<String, HashSet<String>>();
    private HashMap<String,HashSet<String>> unconfirmedAttackerSet=new HashMap<String, HashSet<String>>();

    private volatile int count;
    //the minimum attacker threshold
    private int attackerThreshold=5;
    //the minimum app threshold
    private int appThreshold=20;
    public static void main(String []args){
        Crawler crawler=new AppleItuneCrawler();
        SimpleLS simpleLS=new SimpleLS(crawler);
        simpleLS.run();
    }
    public SimpleLS(Crawler crawler){
        this.crawler=crawler;
        DataPersistence.getInstance().setBaseConfig(SimpleLSConfig.getInst());
        this.attackerThreshold=(int)SimpleLSConfig.getInst().getThresholdAppSize();
        this.appThreshold=(int)SimpleLSConfig.getInst().getThresholdReviewSize();
    }

    public void run(){
        this.loadPresetAbusedApps();
        while(unconfirmedAppSet.size()>0 || unconfirmedAttackerSet.size()>0){
            //retrieve app data for all the unconfirmed set
            for(Iterator<String> appIdIter=unconfirmedAppSet.keySet().iterator();
                    appIdIter.hasNext();){
                String appId=appIdIter.next();
                if(unconfirmedAppSet.get(appId)==null){
                    AppData appData=dataCollection(appId);
                    if(appData==null){
                        log.info("Unable to get data for app "+appId);
                        appIdIter.remove();
                        continue;
                    }
                    //find suspicious reviewers, which are attackers
                    for(Reviewer reviewer:appData.getReviewList()){
                        int abusedAppsCount=0;
                        for(String appId2:reviewer.getAppIdsOrded()){
                            if(confirmedAppSet.contains(appId2)){
                                abusedAppsCount++;
                            }else{
                                HashSet<String> hs=null;
                                if(unconfirmedAttackerSet.containsKey(appId2)){
                                    hs=unconfirmedAttackerSet.get(appId2);
                                }else{
                                    hs=new HashSet<String>();
                                    unconfirmedAttackerSet.put(appId2,hs);
                                }
                                hs.add(reviewer.getReviewerId());
                            }
                        }
                        if(abusedAppsCount>=this.attackerThreshold){
                            confirmedAttackerSet.add(reviewer.getReviewerId());
                            log.info("Find an attacker: "+reviewer.getReviewerId() +" rated apps: "+abusedAppsCount);
                        }
                    }
                }
            }
            unconfirmedAppSet.clear();
            for(String appId:unconfirmedAttackerSet.keySet()){
                int attackerCount=0;
                for(String reviewerId:unconfirmedAttackerSet.get(appId)){
                    if(confirmedAttackerSet.add(reviewerId)){
                        attackerCount++;
                    }
                }
                if(attackerCount>=this.appThreshold){
                    unconfirmedAppSet.put(appId,null);
                    confirmedAppSet.add(appId);
                    log.info("Find an abused app: "+appId +" rated by attackers: "+attackerCount);
                }
            }
            unconfirmedAttackerSet.clear();
        }
    }

    /**
     * Load known abused apps
     */
    public void loadPresetAbusedApps(){
        String seedFilePath=SimpleLSConfig.getInst().getPresetSeedFile();
        if (seedFilePath==null || seedFilePath.trim().equals("")){
            return;
        }
        try{
            File file=new File(seedFilePath);
            if (!file.exists()){
                log.error(" File not exists. "+seedFilePath);
                return;
            }
            BufferedReader br=new BufferedReader(new InputStreamReader(new FileInputStream(file)));
            String line=null;
            while((line=br.readLine())!=null){
                String appId=line.split("::::")[0];
                this.confirmedAppSet.add(appId);
                this.unconfirmedAppSet.put(appId,null);
            }
            br.close();
        }catch (Exception ex){
            log.error(ex.getMessage());
        }
    }
    /**
     * Retrieve data of the app with from DB or Websites
     * @param appIdTemp - the app id to be dealt with
     * @return true -  this app is gonna checked.
     * 		   false - no need to check this app. [popular app or checked app]
     */
    private AppData dataCollection(String appIdTemp){
        AppData appData=null;
        //try to get lock of DB
        try{
            FlushToDBLS.getInstance().getLock();
            appData=DataPersistence.getInstance().loadAppDataFromDB(appIdTemp);
        }finally{
            FlushToDBLS.getInstance().releaseLock();
        }
        if(appData==null){//no record in the database
            appData=new AppData(appIdTemp);
            if(SimpleLSConfig.getInst().isAllowWebpage()){
                boolean appCommentStatus=this.collectAppInfo(appData);
                if(!appCommentStatus){//if false
                    return null;
                }
                this.collectReviewerInfo(appData);
                FlushToDBLS.getInstance().saveObject(appData);
            }else{
                log.error("No Data of app "+appIdTemp);
                return null;
            }

        }

        return appData;
    }
    /**
     * Collect overview,comment,reviewer profile from webpages
     */
    private boolean collectAppInfo(AppData appData){
        try{
            if(SimpleLSConfig.getInst().getSeedStoreLocation()==null){
                log.error("Please specify store location if crawling on the fly!");
                System.exit(0);
            }
            appData.setStoreLocation(SimpleLSConfig.getInst().getSeedStoreLocation());
            OverviewEngine overviewEngine=new OverviewEngine(this.crawler);
            CommentCollector commentCrawler=new CommentCollector(this.crawler);
            String appId=appData.getAppId();
            log.info("Start to retrieve overview of " + appId);
            HttpURLConnection httpURLConnection=this.crawler.getOverviewHttpURLConnection(appId,appData.getStoreLocation());
            if(httpURLConnection==null){
                log.error("Failed to connect the overview page of "+appId);
                return false;
            }
            boolean resultStatus=overviewEngine.retrieveOverview(httpURLConnection,appData);
            if(!resultStatus){//failed to parse overview
                log.error("Failed to parse overview of "+appId);
                return false;
            }
            log.info("Finish retrieving overview of " + appId);
            appData.setUrl(httpURLConnection.getURL().toString());

            //find the total number of pages
            AppData appDataTest=new AppData(appData.getAppId());
            appDataTest.setStoreLocation(appData.getStoreLocation());
            boolean isMoreReview=commentCrawler.retrieveOnePage(appDataTest,(int)SimpleLSConfig.getInst().getPoplarAppReviewerSize());
            if(!isMoreReview){//less than certain page of reviews
                commentCrawler.run(appData);
            }
            if(appData.getCommentList().size()==0){//popular apps
                log.info("Skip popular app "+appId);
                return false;
            }
            return true;
        }catch(Exception ex){
            log.error(ex.getMessage());
            ex.printStackTrace();
            return false;
        }
    }
    /**
     * Collect reviewer's rating history
     */
    private void collectReviewerInfo(final AppData appData){
        final UserProfileEngine upe=new UserProfileEngine(this.crawler);
        this.count=0;
        final ConcurrentLinkedQueue<Comment> tempCommentsList=new ConcurrentLinkedQueue<Comment>();
        final ConcurrentHashMap<String,Thread> threadSet=new ConcurrentHashMap<String,Thread>();
        tempCommentsList.addAll(appData.getCommentList());
        for(int i=0;i<SimpleLSConfig.getInst().getThreadCount();i++){
            final String threadName="UPEng_"+i;
            Thread thread=new Thread(new Runnable(){
                public void run(){
                    String threadName2=threadName;
                    try{
                        while(count<SimpleLSConfig.getInst().getLargestReviewers()){
                            Comment comment=tempCommentsList.poll();
                            if(comment==null){
                                break;
                            }
                            Reviewer reviewer=new Reviewer(comment.getReviewerId(), appData.getStoreLocation());
                            upe.retrieveUserProfile(reviewer);
                            if(reviewer.getReviewSize()>=2){//only collect data for reviewing at least twice
                                appData.getReviewList().add(reviewer);
                            }
                            count++;
                            if(count%1000==0){
                                log.info("The "+count+"th reviewer has already been crawled.");
                            }
                        }
                    }catch(Exception ex){
                        ex.printStackTrace();
                        log.error(ex.getMessage());
                    }finally{
                        threadSet.remove(threadName2);
                    }

                }
            });
            thread.setName(threadName);
            threadSet.put(threadName, thread);
            thread.start();
        }

        try{
            log.info("Start to crawle user review history of app "+appData.getAppId());
            while(threadSet.size()>0){
                Thread.sleep(2000);
            }
            log.info("End of crawling user reivew history of app "+appData.getAppId());
        }catch(Exception ex){
            log.error(ex.getMessage());
        }

    }
}
